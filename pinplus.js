const EventEmitter = require('events');
const debug = require('debug')('PinPlus');
const Frame = require('./frame');
const Processor = require('./processor');

const MAX_CHARACTERISTIC_SIZE = 20;

const DEVICE_POWER_STATE_OFF = 0x30; // '0'
const DEVICE_POWER_STATE_ON = 0x31; // '1'
const DEVICE_POWER_STATE_OFF_UNSECURED = 0x33; // '3'
const DEVICE_POWER_STATE_ON_UNSECURED = 0x34; // '4'

class PinPlus {
  constructor(name = 'SumUp 000') {
    this.name = name;

    this.sendMessage = this.sendMessage.bind(this);
    this.receiveConfimation = this.receiveConfimation.bind(this);

    this.incompleteMessage = Buffer.alloc(0);
    this.sendReady = true;
    this.sendQueue = Buffer.alloc(0);

    this.power = Buffer.from([DEVICE_POWER_STATE_ON_UNSECURED]);
    this.emitter = new EventEmitter();
    this.processor = new Processor(this);
  }

  setUpdateCallback(characteristic, updateValueCallback, maxValueSize) {
    if (characteristic === 'power') {
      updateValueCallback(this.power);
    }
    this.emitter.on(characteristic, updateValueCallback);
  }

  write(chunk) {
    //debug('write', chunk.toString('hex'));
    this.incompleteMessage = Buffer.concat([this.incompleteMessage, chunk]);

    if (Frame.isComplete(this.incompleteMessage)) {
      debug('complete', this.incompleteMessage.toString('hex'));

      const protocolMessage = Frame.getProtocolMessage(this.incompleteMessage);
      this.incompleteMessage = Buffer.alloc(0);
      this.sendMessage(
        new Frame(
          protocolMessage.sequenceNumber,
          protocolMessage.ack(),
        ).serialize(),
      );
      const response = this.processor.process(protocolMessage);
      if (response) {
        this.sendMessage(
          new Frame(protocolMessage.sequenceNumber, response).serialize(),
        );
      }
    }
  }

  sendMessage(data) {
    this.sendQueue = Buffer.concat([this.sendQueue, data]);
    if (this.sendReady) {
      this.sendReady = false;
      this.sendChunk();
    }
  }

  sendChunk() {
    while (
      this.sendQueue.length > 0 &&
      this.emitter.listenerCount('read') > 0
    ) {
      const end = Math.min(MAX_CHARACTERISTIC_SIZE, this.sendQueue.length);
      const chunk = this.sendQueue.slice(0, end);
      //debug('send chunk', chunk.toString('hex'));
      this.emitter.emit('read', chunk);
      this.sendQueue = this.sendQueue.slice(end);
    }
  }

  receiveConfimation() {
    //debug('sendReady false -> true');
    this.sendReady = true;
  }
}

module.exports = PinPlus;

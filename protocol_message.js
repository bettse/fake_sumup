const debug = require('debug')('ProtocolMessage');

class ProtocolMessage {
  constructor(sequenceNumber, buffer) {
    this.sequenceNumber = sequenceNumber;
    this.moduleId = buffer[0];
    this.commandId = buffer[1];
    this.content = buffer.slice(2);
  }

  ack() {
    return Buffer.from('80010000', 'hex');
  }

  emptyResponse(status = 0) {
    const {moduleId, commandId} = this;
    const response = Buffer.alloc(4);
    response[0] = moduleId;
    response[1] = commandId;
    response.writeUInt16BE(status, 2);
    return response;
  }
}

module.exports = ProtocolMessage;

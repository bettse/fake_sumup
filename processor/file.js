const fs = require('fs');
const path = require('path');
const debug = require('debug')('Processor:File');
const ASN1 = require('@lapo/asn1js');

const moduleId = 2;

const devCertProd = fs.readFileSync(path.join('certs', 'dev_cert_prod.bin'));
const devCertRD = fs.readFileSync(path.join('certs', 'dev_cert_rd.bin'));

function getCerts(protocolMessage) {
  const {moduleId, commandId} = protocolMessage;
  const status = 0;

  const a = Buffer.from('110063a9417d34200000', 'hex');
  const b = Buffer.from('270063a9417d34200000', 'hex');
  const suffix = Buffer.concat([a, b]);

  // 2 byte status
  // 2 byte module/command
  // 2 byte length of cert
  // cert
  // 2 byte length of cert
  // cert
  const content = Buffer.alloc(
    2 + 2 + 2 + devCertProd.length + 2 + devCertRD.length + suffix.length,
  );

  content[0] = moduleId;
  content[1] = commandId;
  content.writeUInt16BE(status, 2);
  var start = 4;
  content.writeUInt16BE(devCertProd.length, start);
  start += 2;
  devCertProd.copy(content, start);
  start += devCertProd.length;
  content.writeUInt16BE(devCertRD.length, start);
  start += 2;
  devCertRD.copy(content, start);
  start += devCertRD.length;
  suffix.copy(content, start);

  return content;
}

let partialCert = [];
let total_length = 0;
function assembleMessage(payload) {
  const index = payload[0];
  let start = 1;
  const piece_length = payload.readUInt16BE(start);
  start += 2;
  if (index === 0) {
    total_length = payload.readUInt16BE(start);
    start += 2;
  }

  const piece = payload.slice(start, start + piece_length);
  partialCert[index] = piece;
  const full = Buffer.concat([...partialCert]);

  if (full.length === total_length) {
    partialCert = [];
    total_length = 0;
    return full;
  }
  //console.log(`incomplete cert ${full.length} / ${total_length}`);
  return null;
}

function setCerts(protocolMessage) {
  const content = assembleMessage(protocolMessage.content);
  //incomplete cert message
  if (!content) {
    return protocolMessage.emptyResponse();
  }

  //A0 09 6B 1T X0 1N 01 00 KS 18
  //11 01 63 A9 41 7D 34 20 00 00
  //347B24EC0AFADB0E3CA45FEA661E68A6A4FCA6BB78AC6C40CCA49773
  const ascii = content.slice(0, 20).toString('ascii');
  const cert_thing = content.slice(20, 20 + 20).toString('ascii');
  const random = content.slice(40, 0x60).toString('ascii');
  const misc = content.slice(0x60, 0x260);
  const unknown = content.slice(0x260, 0x262);
  // 2 bytes are the same, but don't seem to be part of the cert

  const cert = ASN1.decode(content.slice(0x262));
  console.log(cert.toPrettyString());

  fs.writeFileSync(
    path.join('certs', `cert-${cert_thing}.bin`),
    Buffer.from(cert.toHexString(), 'hex'),
  );

  fs.writeFileSync(path.join('certs', `misc-${cert_thing}.bin`), misc);

  debug({
    what: 'setCerts',
    ascii,
    cert_thing,
    random,
    misc: misc.toString('hex'),
    unknown: unknown.toString('hex'),
    cert: 'saved to filesystem',
    //cert: cert.toPrettyString(),
  });
  return protocolMessage.emptyResponse();
}

let bin_length = 0;
function prepareFileLoad(protocolMessage) {
  const {content} = protocolMessage;
  bin_length = content.readUInt32BE(content.length - 4);
  const other_length = content.readUInt32BE(content.length - 8);
  const length = content.readUInt32LE(content.length - 12);
  const body = content.slice(2, 2 + length); //?

  const prefix = content.slice(0, 2);
  const suffix = content.slice(2 + length, content.length - 12);

  const delta = other_length - bin_length;
  debug({
    what: 'prepareFileLoad',
    prefix,
    body: body.toString('hex'),
    blength: body.length,
    bin_length,
    other_length,
    length,
    delta,
    suffix,
  });
  return protocolMessage.emptyResponse();
}

let bin_pieces = [];
function fileLoad(protocolMessage) {
  const {content} = protocolMessage;
  const index = content[0];
  const length = content.readUInt16BE(1);
  const piece = content.slice(3, 3 + length);
  bin_pieces[index] = piece;
  const bin = Buffer.concat([...bin_pieces]);
  if (bin.length < bin_length) {
    return protocolMessage.emptyResponse();
  }

  debug({
    what: 'fileLoad',
    fileLength: bin.length,
    file: bin.toString('hex'),
  });

  return protocolMessage.emptyResponse();
}

function enterProtectedMode(protocolMessage) {
  const {content} = protocolMessage;

  debug({
    what: 'enterProtectedMode',
    content,
  });

  //enterProtectedMode response = 27 01 63 A9 41 7D 34 20 00 01

  return protocolMessage.emptyResponse();
}

function leaveProtectedMode(protocolMessage) {
  const {content} = protocolMessage;

  debug({
    what: 'leaveProtectedMode',
    content,
  });

  return protocolMessage.emptyResponse();
}

module.exports = {
  moduleId,
  processors: [
    {
      commandId: 1,
      fun: enterProtectedMode,
    },
    {
      commandId: 2,
      fun: leaveProtectedMode,
    },

    {
      commandId: 4,
      fun: getCerts,
    },
    {
      commandId: 5,
      fun: setCerts,
    },
    {
      commandId: 8,
      fun: prepareFileLoad,
    },
    {
      commandId: 9,
      fun: fileLoad,
    },
  ],
};

const debug = require('debug')('Processor:Device');
const moduleId = 1;

// Treating this like a blob, but I can see an argument for making it semantic (a dict)
const deviceInfo = Buffer.from(
  'c10401000142c20401000000c304322e3030c404ffffffffc510b26786210800d6affe09080cad000000c604ffffffffc70563a9417d34c804ffffffffc90120ca020800cb0100cc0161cd01ffce0403001362',
  'hex',
);

const deviceData = [
  null,
  '01000142',
  '01000000',
  '322e3030',
  'ffffffff',
  'b26786210800d6affe09080cad000000',
  'ffffffff',
  '63a9417d34',
  'ffffffff',
  '20',
  '0800',
  '00',
  '61',
  'ff',
  '03001362',
];

function getDeviceInfo(protocolMessage) {
  const {moduleId, commandId} = protocolMessage;
  const status = 0;
  const content = Buffer.alloc(2 + 2 + deviceInfo.length);

  /*
  deviceData
    .map((str, i) => {
      if (str) {
        const buffer = Buffer.from(str, 'hex');
        const header = Buffer.from([i + 0xc0, buffer.length]);
        return Buffer.concat([header, buffer]);
      }
      return str;
    })
    .filter(x => x !== null);
  const body = Buffer.concat(deviceData);
  */

  content[0] = moduleId;
  content[1] = commandId;
  content.writeUInt16BE(status, 2);
  deviceInfo.copy(content, 4);
  return content;
}

function ten(protocolMessage) {
  const {content} = protocolMessage;
  debug({what: '1/10', content});
  return protocolMessage.emptyResponse();
}

module.exports = {
  moduleId,
  processors: [
    {
      commandId: 1,
      fun: getDeviceInfo,
    },
    {
      commandId: 10,
      fun: ten,
    },
  ],
};

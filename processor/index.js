const debug = require('debug')('Processor');

const device = require('./device');
const file = require('./file');

class Processor {
  constructor(pinplus) {
    this.pinplus = pinplus;
    this.processors = [];

    [device, file].forEach(mod => {
      const {processors, moduleId} = mod;
      processors.forEach(processor => {
        const {commandId, fun} = processor;
        this.registerProcessor(moduleId, commandId, fun);
      });
    });
  }

  process(protocolMessage) {
    const {moduleId, commandId} = protocolMessage;
    const processor = this.getProcessor(moduleId, commandId);
    return processor(protocolMessage);
  }

  getProcessor(moduleId, commandId) {
    const moduleProcessors = this.processors[moduleId];
    if (moduleProcessors) {
      return moduleProcessors[commandId] || console.log;
    }
    return console.log;
  }

  // Currently once supports once
  // Does this feel somewhat like redux?
  registerProcessor(moduleId, commandId, processor) {
    this.processors[moduleId] = this.processors[moduleId] || [];
    this.processors[moduleId][commandId] = processor;
  }
}

module.exports = Processor;

const {Characteristic} = require('@abandonware/bleno');
const debug = require('debug')('DeviceService:PowerCharacteristic');

class PowerCharacteristic extends Characteristic {
  constructor(pinplus) {
    super({
      uuid: '22ffc5471bef48e2aa87b87e23ac0bbd',
      properties: ['read', 'notify'],
    });
    this.pinplus = pinplus;
  }

  onSubscribe(maxValueSize, updateValueCallback) {
    debug('subscribe', maxValueSize);
    this.pinplus.setUpdateCallback('power', updateValueCallback, maxValueSize);
  }

  onUnsubscribe() {
    debug('unsubscribe');
    this.pinplus.setUpdateCallback('power', () => {});
  }

  onNotify() {
    debug('onNotify');
  }

  onReadRequest(offset, callback) {
    debug('onReadRequest');
    if (offset) {
      console.log('onReadRequest with offset unsupported');
      callback(this.RESULT_INVALID_OFFSET);
    } else {
      callback(this.RESULT_SUCCESS, Buffer.alloc(0));
    }
  }
}

module.exports = PowerCharacteristic;

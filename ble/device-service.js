const {PrimaryService} = require('@abandonware/bleno');

const readCharacteristic = require('./read-characteristic');
const writeCharacteristic = require('./write-characteristic');
const powerCharacteristic = require('./power-characteristic');
const wakeCharacteristic = require('./wake-characteristic');

class DeviceService extends PrimaryService {
  constructor(pinplus) {
    super({
      uuid: 'd839fc3c84dd4c369126187b07255129',
      characteristics: [
        new readCharacteristic(pinplus),
        new writeCharacteristic(pinplus),
        new powerCharacteristic(pinplus),
        new wakeCharacteristic(pinplus),
      ],
    });
    this.pinplus = pinplus;
  }
}

module.exports = DeviceService;

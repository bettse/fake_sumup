const {Characteristic} = require('@abandonware/bleno');
const debug = require('debug')('DeviceService:WakeCharacteristic');

class WakeCharacteristic extends Characteristic {
  constructor(pinplus) {
    super({
      uuid: 'f953144be33a4079b202e3d7c1f3dbb0',
      properties: ['write', 'writeWithoutResponse'],
    });
    this.pinplus = pinplus;
  }

  onWriteRequest(data, offset, withoutResponse, callback) {
    debug('onWriteRequest');
    //callback(this.RESULT_INVALID_ATTRIBUTE_LENGTH);
    if (offset) {
      console.log('onWriteRequest with offset unsupported');
      callback(this.RESULT_ATTR_NOT_LONG);
    } else {
      this.pinplus.wake(data, offset);
      callback(this.RESULT_SUCCESS);
    }
  }
}

module.exports = WakeCharacteristic;

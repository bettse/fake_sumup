const {Characteristic} = require('@abandonware/bleno');
const debug = require('debug')('DeviceService:WriteCharacteristic');

class WriteCharacteristic extends Characteristic {
  constructor(pinplus) {
    super({
      uuid: 'b378db854ec34daa828e1b99607bd6a0',
      properties: ['write', 'writeWithoutResponse'],
    });
    this.pinplus = pinplus;
  }

  onWriteRequest(data, offset, withoutResponse, callback) {
    debug('onWriteRequest');
    //callback(this.RESULT_INVALID_ATTRIBUTE_LENGTH);
    if (offset) {
      console.log('onWriteRequest with offset unsupported');
      callback(this.RESULT_ATTR_NOT_LONG);
    } else {
      this.pinplus.write(data);
      callback(this.RESULT_SUCCESS);
    }
  }
}

module.exports = WriteCharacteristic;

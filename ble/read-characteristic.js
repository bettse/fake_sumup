const {Characteristic} = require('@abandonware/bleno');
const debug = require('debug')('DeviceService:ReadCharacteristic');

class ReadCharacteristic extends Characteristic {
  constructor(pinplus) {
    super({
      uuid: '1f6b14c997fa4f1eaaa67e152fdd04f4',
      properties: ['read', 'notify', 'indicate'],
    });
    this.pinplus = pinplus;
  }

  onSubscribe(maxValueSize, updateValueCallback) {
    debug('subscribe', maxValueSize);
    this.pinplus.setUpdateCallback('read', updateValueCallback, maxValueSize);
  }

  onUnsubscribe() {
    debug('unsubscribe');
    this.pinplus.setUpdateCallback('read', () => {});
  }

  onNotify() {
    debug('onNotify');
    this.pinplus.receiveConfimation();
  }

  onIndicate() {
    debug('onIndicate');
    this.pinplus.receiveConfimation();
  }

  onReadRequest(offset, callback) {
    debug('onReadRequest');
    if (offset) {
      console.log('onReadRequest with offset unsupported');
      callback(this.RESULT_INVALID_OFFSET);
    } else {
      callback(this.RESULT_SUCCESS, Buffer.alloc(0));
    }
  }
}

module.exports = ReadCharacteristic;

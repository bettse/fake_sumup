process.env['BLENO_DEVICE_NAME'] = 'SumUp';
const bleno = require('@abandonware/bleno');
const DeviceService = require('./ble/device-service');
const PinPlus = require('./pinplus');

function main() {
  const pinplus = new PinPlus();
  const deviceService = new DeviceService(pinplus);

  bleno.on('stateChange', state => {
    console.log('on -> stateChange:', state);

    if (state === 'poweredOn') {
      bleno.startAdvertising(pinplus.name, [deviceService.uuid]);
    } else {
      bleno.stopAdvertising();
    }
  });

  bleno.on('advertisingStart', function(error) {
    console.log('on -> advertisingStart:', error ? 'error' + error : 'success');

    if (!error) {
      bleno.setServices([deviceService]);
    }
  });
}

main();

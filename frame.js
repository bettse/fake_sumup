const {crc16ccitt} = require('crc');
const debug = require('debug')('Frame');
const ProtocolMessage = require('./protocol_message');

const stx = 0x02;
const etx = 0x03;

class Frame {
  constructor(sequenceNumber, content) {
    this.sequenceNumber = sequenceNumber;
    this.content = content;
  }

  serialize() {
    const buffer = Buffer.alloc(1 + 2 + 1 + this.content.length + 2 + 1);
    buffer[0] = stx;
    buffer.writeUInt16BE(this.content.length, 1);
    buffer[3] = this.sequenceNumber;
    this.content.copy(buffer, 4);

    const crc = crc16ccitt(buffer.slice(1, 1 + 2 + 1 + this.content.length), 0);

    buffer.writeUInt16BE(crc, 4 + this.content.length);
    buffer[4 + this.content.length + 2] = etx;
    return buffer;
  }

  static isComplete(data = tuffer.alloc(0)) {
    if (data.length < 3) {
      return false;
    }

    if (data[0] !== stx) {
      return false;
    }

    const header = data.slice(1, 4);
    let length = header.readUInt16BE(0);
    if (length > 0x8000) {
      length -= 0x8000;
    }
    if (data.length < 4 + length + 3) {
      return false;
    }

    return true;
  }

  static getProtocolMessage(data = Buffer.alloc(0)) {
    if (!Frame.isComplete(data)) {
      console.log('incomplete frame');
      return null;
    }
    const header = data.slice(1, 4);
    let length = header.readUInt16BE(0);
    if (length > 0x8000) {
      length -= 0x8000;
      protect = true;
    }

    const sequenceNumber = header[2];
    const payload = data.slice(4, 4 + length);
    const calcCrc = data.readUInt16BE(1 + 3 + length);
    const crc = crc16ccitt(data.slice(1, 4 + length), 0);

    if (crc !== calcCrc) {
      console.log('crc mismatch', crc, calcCrc);
      return null;
    }

    return new ProtocolMessage(sequenceNumber, payload);
  }
}

module.exports = Frame;
